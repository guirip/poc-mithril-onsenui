#!/bin/sh
# GR 09/11/2016

# font styles
bold="\033[1m"
normal="\033[0m"


pathFile="mobigeopath.txt"
destination=www/lib

log()
{
	echo
	echo "[updater] "$bold$@$normal
	echo
}

getInputPath()
{
	if [ -f "${pathFile}" ]
	then
		mobigeoPath=$(cat $pathFile)
		proceed
	else
		while true; do
	    	read -p "Please enter the path of mobigeo web folder (e.g /var/www/projets/mobigeo-sdk-ca-sqy/web): " mobigeoPath
	    	echo $mobigeoPath > $pathFile
	    	proceed
	    done
	fi
}

proceed()
{
	# check input value
	if ! [ -d "${mobigeoPath}" ]
	then
		log "Update failure. This is not a directory ${mobigeoPath}"
		exit 1
	fi

	# build
	log "Building web plugin archive..."
	cd $mobigeoPath && \
	./build.sh zipPluginWeb && \
	cd - > /dev/null && \

	# copy zip
	log "Copying zip..." && \
	cp $mobigeoPath/*-web.zip $destination && \

	# clear existing mobigeo directory
	log "Removing existing 'mobigeo' directory..." && \
	rm -Rf $destination/mobigeo && \

	# unzip
	log "Unzippping to ${destination}..." && \
	unzip -o $destination/*-web.zip -d $destination && \

	# delete zip
	log "Removing archive..." && \
	rm -f $destination/*-web.zip


	status=$?
	if [ $status -gt 0 ]
	then
		log "Failure while updating mobigeo"
	else
		log "Update successful 😇"
	fi
	exit $status
}


# Run
getInputPath