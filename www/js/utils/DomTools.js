/**
 * Licenced to Mobile-Spot
 *
 * DOM Tool module
 */
var Dt = (function() {
	'use strict';

	var findParentNodeWithNodeName = function(element, nodeName) {

		if (element === null || typeof element !== 'object') {
			return null;
		}
		if (typeof nodeName !== 'string') {
			MobiGeoDebug.error(module, 'className parameter is not valid: ' + nodeName);
			return null;
		}

		if (element.nodeName.toLowerCase() === 'body') {
			return null;
		}

		if (element.nodeName.toLowerCase() === nodeName.toLowerCase()) {
			return element;
		} else {
			return findParentNodeWithNodeName(element.parentNode, nodeName);
		}
	};

	var findParentNodeWithAttribute = function(element, attributeName) {

		if (element === null || typeof element !== 'object') {
			return null;
		}
		if (typeof attributeName !== 'string') {
			console.error('attributeName parameter is not valid: ' + attributeName);
			return null;
		}
		if (typeof element.getAttribute !== 'function') {
			return null;
		}

		if (element.getAttribute(attributeName) !== null) {
			return element;
		} else {
			return findParentNodeWithAttribute(element.parentNode, attributeName);
		}
	};

	/**
	 * @param  {dom element} el
	 * @param  {function} filter (optional)
	 * @return {array}
	 */
	var getSiblings = function(el, filter) {

		var siblings = [],
			sibling = el.parentNode.firstChild;
	    do {
	    	if (sibling !== el && (!filter || filter(sibling))) {
	    		siblings.push(sibling);
	    	}
	    	sibling = sibling.nextSibling;
	    } while (sibling !== null);

	    return siblings;
	};


	// Expose public members
	return {
		findParentNodeWithNodeName : findParentNodeWithNodeName,
		findParentNodeWithAttribute: findParentNodeWithAttribute,
		getSiblings                : getSiblings,
	};
})();