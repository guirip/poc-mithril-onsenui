
var Jt = (function(){

	var isEmpty = function(arg) {
		return arg === null || typeof arg === 'undefined';
	};

	/**
	 * Return the attribute value of an object from a path
	 * @param {object|array} obj
	 * @param {string} attr, example: "user.address.street"
	 * @return {*}
	 */
	var getValueFromStringAttribute = function(obj, attr) {
		if (isEmpty(obj)) {
			return null;
		}
		if (typeof attr !== 'string'){
			throw new Error('Invalid argument! expected "attr" to be a string, but got ' + typeof attr);
		}
		var path = attr.split('.');

		// Simple case
		if (path.length === 1){
			return obj[path];
		}
		else {
			// Case with sub-levels
			var tmpValue = obj[path.shift()];
			var step;
			while (isEmpty(step = path.shift()) === false){
				if (isEmpty(tmpValue) === true || tmpValue.hasOwnProperty(step) === false){
					return null;
				}
				tmpValue = tmpValue[step];
			}
			return tmpValue;
		}
	};

	var filterObjects = function(objs, criterias, filterOne){
		var matched = [];
		if (isEmpty(objs)){
			return matched;
		}

		for (var i = 0; i < objs.length; i++) {

			if (matchCriterias(objs[i], criterias) === true){
				matched.push(objs[i]);

				// Handle when only one result is wanted
				if (matched.length === 1 && filterOne === true) {
					return matched;
				}
			}
		}
		return matched;
	};

	var matchCriterias = function(obj, criterias) {
		if (isEmpty(criterias) === true){
			return true;
		}

		// Default assertion is true
		var doesMatch = true;

		// Check if each criteria match
		for (var criteria in criterias){
			if (doesMatch === false){
				// No need to check the other criterias
				break;
			}
			if (criterias.hasOwnProperty(criteria) === false){
				continue;
			}

			// Special 'not' criterias
			if (criteria === 'not') {
				var notCriterias = criterias[criteria];
				for (var notCriteria in notCriterias){
					if (notCriterias.hasOwnProperty(notCriteria) === false){
						continue;
					}
					if (getValueFromStringAttribute(obj, notCriteria) === notCriterias[notCriteria]){
						doesMatch = false;
					}
				}
			}
			// Default (yes) criteria
			else if (getValueFromStringAttribute(obj, criteria) !== criterias[criteria]){
				doesMatch = false;
			}
		}
		return doesMatch;
	};

	/**
	 * Filter objets and stop when one is found
	 * @param  {object} obj
	 * @param  {object} criterias
	 * @return {object}
	 */
	var filterOne = function(obj, criterias) {
		var results = filterObjects(obj, criterias, true);
		return results.length > 0 ? results[0] : null;
	};

    var objectToArray = function(obj) {
        var array = [];
        for (var key in obj) {
            if (obj.hasOwnProperty(key) === true) {
            	array.push(obj[key]);
            }
        }
        return array;
    };


	return {
		getValueFromStringAttribute: getValueFromStringAttribute,
		filterObjects : filterObjects,
		filterOne     : filterOne,
		objectToArray : objectToArray,
		isEmpty       : isEmpty,
	};

})();