
var Log = (function(){
	var stackOnDebug   = false,
		showCallerOnly = false,
		stackSeparator = 'at ';

	var getStack = function(stack) {
		// Skip this function call
		var firstSepIndex = stack.indexOf(stackSeparator);
		if (firstSepIndex !== -1) {
			stack = stack.substr(stack.indexOf(stackSeparator, firstSepIndex+stackSeparator.length));
		}
		if (showCallerOnly) {
			var nextSepIndex = stack.indexOf(stackSeparator, stackSeparator.length);
			if (nextSepIndex !== -1) {
				stack = stack.substr(0, nextSepIndex);
			}
		}
		return stack;
	};

	var debug = function() {
		var stack = stackOnDebug ? getStack(new Error().stack) : null;
		console.log('[debug] ' + getParamsValueAsString(Array.prototype.slice.call(arguments)) + (stack ? '\n    '+stack : ''));
	};

	var warn = function() {
		var stack = getStack(new Error().stack);
		console.log('[WARN] ⚠ ' + getParamsValueAsString(Array.prototype.slice.call(arguments)) + '\n    '+stack);
	};

    /**
     * Return a string representation of arguments
     * @return {Array} params
     * @private
     */
    var getParamsValueAsString = function(params) {
        return (typeof params === 'undefined' || params === null ? '' : params.map(argToString).join(' | '));
    };

    /**
     * Get string representation of argument
     * @param  {*} arg
     * @return {String|primitive}
     * @private
     */
    var argToString = function(arg) {
        if (typeof arg === 'object') {
            return JSON.stringify(arg);
        } else {
            return arg;
        }
    };

	return {
		debug: debug,
		warn : warn,

		get stackOnDebug() {
			return stackOnDebug;
		},
		set stackOnDebug(value) {
			stackOnDebug = value;
		},
		get showCallerOnly() {
			return showCallerOnly;
		},
		set showCallerOnly(value) {
			showCallerOnly = value;
		},
	};
})();