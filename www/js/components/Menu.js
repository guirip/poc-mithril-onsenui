
var Menu = (function() {

	var menuOptions = { id:'menu', width:'220px', collapse:'', swipeable:'', side:'right'};

	/**
	 * @param  {Vnode}  vnode
	 * @param  {object} options
	 * @return {Vnode}
	 */
	var wrap = function(vnode, options) {

		// Process options
		var hasOptions = options !== null && typeof options !== 'undefined';
		if (hasOptions && options.swipeable === false) {
			delete menuOptions.swipeable;
		} else {
			menuOptions.swipeable = '';
		}

		return  m('ons-splitter', [
				  m('ons-splitter-side', menuOptions, [
				    m('ons-page', [
					  m('ons-list', [
						iterateOnPages(function(pageKey) {

							var attributes = {
								//href	: Pages[pageKey].path,
								onclick : function(e){
									Menu.toggle(e.target);
									Router.go(Pages[pageKey]);
									//m.route.link
								}
							};
							if (hasOptions && options.page === Pages[pageKey].id) {
								attributes.disabled = '';
							} else {
								attributes.tappable = '';
							}

							return  m('ons-list-item', attributes, Pages[pageKey].label);
						})
					  ]),
				    ]),
				  ]),
				  m('ons-splitter-content', [ vnode ]),
			    ]);
	};

	var toggle = function(el) {
		Jt.filterOne(Dt.findParentNodeWithNodeName(el, 'ons-splitter').children, { nodeName: 'ONS-SPLITTER-SIDE' }).toggle();
	};


	return {
		wrap  : wrap,
		toggle: toggle,
	};

})();