
var Router = (function() {

	var currentPath;

	/**
	 * Navigate to a page
	 * @param {object} page (@see Pages object)
	 * @param {object} options
	 */
	var go = function(page, options) {
		var delayed = false;
		if (page.path !== currentPath) {

			// Hide all
			iterateOnPages(function(key) {
				Pages[key].vnode.dom.style.display = 'none';
				Pages[key].active = false;
			});

			// Show demanded page
			if (!page.isMounted) {
				m.mount(page.vnode.dom, page.component);
				page.isMounted = true;
				delayed = true;
			}
			page.vnode.dom.style.display = 'block';
			page.active = true;
			currentPath = page.path;
		}

		if (typeof page.component.onShow === 'function') {
			var callOnShow = function(){
				page.component.onShow(options);
			};
			if (delayed) {
				window.requestAnimationFrame(callOnShow);
			} else {
				callOnShow();
			}
		}
	};

	/**
	 * For initial app load, parse querystring to determine the page to display
	 */
	var run = function() {
		// Parse queryString to get the route (if any)
		var separatorIndex = window.location.href.indexOf('?');
		if (separatorIndex !== -1) {
			var query = m.parseQueryString(window.location.href.substring(separatorIndex)),
				pageToDisplay;
			for (var key in query) {
				if (key.startsWith('/')){
					pageToDisplay = Jt.filterOne(
						iterateOnPages(function(key){ return Pages[key]}),
						{ path: key }
					);
					continue;
				}
			}
		}
		if (typeof pageToDisplay === 'undefined') {
			pageToDisplay = defaultPage;
		}
		go(pageToDisplay);
	};


	return {
		go : go,
		run: run,
	};
})();