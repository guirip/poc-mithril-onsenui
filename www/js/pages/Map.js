
var Map = (function() {

	var pageConfig = {
			id       : 'page-map',
			name     : 'Map',
			path     : '/map',
			label    : 'Carte', // i18n TODO
		},
		mapContainerVnode,
		areEventsBinded = false,
		isMapLoaded     = false,
		isMapReady      = false,
		queuedAction    = [],
		currentOptions;

	var bindEventHandlers = function() {
		MobiGeo.Map.on('ready', function mapReady() {
			isMapReady = true;
			Log.debug('Map has successfully been loaded');
			if (queuedAction.length > 0) {
				queuedAction.pop()();
			}
		});

		MobiGeo.Map.POI.on('tap', function poiTapped(data) {
			Log.debug('POI selected', data);
		});

		var commonErrorHandler = function(error) {
			console.error('MobiGeo encountered an error: ', error);
		};
		MobiGeo.on('error', commonErrorHandler);
		MobiGeo.Map.on('error', commonErrorHandler);
		MobiGeo.Map.Route.on('error', commonErrorHandler);

		areEventsBinded = true;
	};

	var loadDataset = function() {
		if (!areEventsBinded) {
			bindEventHandlers();
		}
		if (!isMapLoaded && MobiGeo.load('MeL8ooso') === true) {
			MobiGeo.Map.create(mapContainerVnode.dom, { showMapTitle: false });
			isMapLoaded = true;
		}
		applyOptions();
	};

	/**
	 * Apply options from querystring (e.g poiId to show)
	 */
	var applyOptions = function() {
		if (typeof currentOptions !== 'undefined') {
			var action;

			// Show a POI
			if (typeof currentOptions.poiId !== 'undefined') {
				var poiId = currentOptions.poiId;
				action = function() {
					MobiGeo.Map.POI.show(poiId);
				};
			}

			// Perform the action, or queue it if mobigeo is not ready yet
			if (typeof action === 'function') {
				if (isMapReady) {
					action();
				} else {
					queuedAction.push(action);
				}
			}
		}
	};

	/**
	 * define the view
	 * @return
	 */
	var view = function(vnode) {
		Log.debug('generate Map vnode');

		currentOptions = vnode.attrs;
		mapContainerVnode = m('div', { id: 'map-container', /*oncreate: loadDataset*/ });

		var mapVnode =
			Menu.wrap([
					mapContainerVnode,
					m('ons-fab', {
						ripple:'',
						position:'top right',
						onclick: function(e){ Menu.toggle(e.target) }
					}, [
						m('ons-icon', {
							icon:'md-menu',
						})
					]),
				],
				// menu options
				{ swipeable: false, page: pageConfig.id }
			);
		return mapVnode;
	};

	var onShow = function(options) {
		Log.debug('on show ' + pageConfig.id);
		currentOptions = options;
		loadDataset();
	};

	return {
		get pageConfig() {
			return pageConfig;
		},
		view     : view,
		onShow   : onShow,
	};
})();
