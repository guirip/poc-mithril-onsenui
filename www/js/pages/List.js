
var List = (function(){

	var pageConfig = {
			id       : 'page-list',
			name     : 'List',
			path     : '/list',
			label    : 'Liste', // i18n TODO

			// TEMP
			isDefault: true,
		},
		data = null,
		isDataReady = false,

		dataConfigs = {
			sqy: {
				url: 'lib/mobigeo/data/sqypark/db_fr.json',
				field: 'services',
				filterDataToUse: function(_data) {
					return _data[dataConfigs.sqy.field].data;
				},
				getDataToDisplay: function(row) {
					return row[1] + ' - ' + row[2]
				},
			},
			wgc: {
				url: 'lib/wgc/datas.txt',
				field: 'visitors',
				filterDataToUse: function(_data) {
					return _data[dataConfigs.wgc.field].data;
				},
				getDataToDisplay: function(row) {
					return row[1] ? row[1] + '<small>&nbsp;- '+row[2]+'</small>' : null;
				},
			},
		},
		dataConfig = dataConfigs.wgc;

	/**
	 *
	 */
	var fetchData = function() {
		m.request({
			method: 'GET',
			url   : dataConfig.url
		})
		.then(function(_data) {
			Log.debug('Loaded ' + dataConfig.url);
			isDataReady = true;
			setData(Jt.objectToArray(dataConfig.filterDataToUse(_data)));
		})
		.catch(function() {
			console.error('failed to load data', arguments);
			isDataReady = false;
			setData([]);
		});
	};

	var setData = function(value) {
		var timeKey = 'list.update.data'
		console.time(timeKey);

		data = value;
		m.redraw();

		console.timeEnd(timeKey);
	};

	/**
	 * define the view
	 * @return vnode
	 */
	var view = function(vnode) {
		Log.debug('generate List vnode');

		return  Menu.wrap(
					Skeleton.wrap(
						m('ul[id=page-list-ul]', [
						//m('ons-list', { onclick: handleElementSelected }, [
							data === null ? m('li.loader', { style: {marginTop: '5%', width: '100%'}}, m('ons-progress-circular[indeterminate]', { style: {margin: 'auto', display: 'flex'}})) :

								data.map(function(row) {
									/*return  m('ons-list-item', {
											'data-originalid': data()[key][1],
											modifier: 'lowheight chevron',
											//ripple: '',
										}, [
											//m("div", m.trust(dataConfig.getDataToDisplay(data()[key])))
											m("div.list__item__left", m.trust(dataConfig.getDataToDisplay(data()[key]))),
										]);*/
									var label = dataConfig.getDataToDisplay(row);
									return label ? m('li', m.trust(label)) : null;
								}),
						])
					, { title: dataConfig.field.substring(0,1).toUpperCase() + dataConfig.field.substring(1) }),
					{ page: pageConfig.id }
				);
	};

	var handleElementSelected = function(e) {
		var listItemEl = Dt.findParentNodeWithNodeName(e.target, 'ons-list-item');
		if (listItemEl) {
			var poiId = listItemEl.getAttribute('data-originalid');
			Router.go(Pages.Map, { poiId: poiId });
		}
	};

	var onShow = function(options) {
		Log.debug('on show ' + pageConfig.id);
		if (!isDataReady) {
			fetchData();
		}
	};


	var TestBinding = (function() {
		var toUpperCase = false;

		return {
			run: function() {
				setData(data.map(function(row) {
					if (toUpperCase) {
						row[1] = row[1].toUpperCase();
					} else {
						row[1] = row[1].toLowerCase();
					}
					return row;
				}));
				toUpperCase = !toUpperCase;
			},
		};
	})();


	return {
		get pageConfig() {
			return pageConfig;
		},
		fetchData  : fetchData,
		testBinding: TestBinding.run,
		view       : view,
		onShow     : onShow,
	};
})();
