

var setup = function() {

	Log.debug('setting routes');
	m.route.prefix('?');

	// Create pages containers
	var pages = [];
	iterateOnPages(function(pageKey) {
		Pages[pageKey].vnode = m('div', { id: Pages[pageKey].id, style: {display: 'none'} });
		pages.push(Pages[pageKey].vnode);
	});

	// Add these containers to 'body' element
	var pagesContainerVnode = m('div', { id:'pages-container' });
	m.render(document.body, pagesContainerVnode);
	m.render(pagesContainerVnode.dom, pages);

	Router.run();
};


if (document.readyState === "complete" || document.readyState === "loaded") {
	// Dom ready yet
	setup();
} else {
	// Dom not ready yet
	document.addEventListener('DOMContentLoaded', setup);
}
